/*
 * Thomas Roos
 * ID: 2139198
 */

package linearalgebra;

public class Vector3d {
    private double x;
    private double y;
    private double z;

    // Constructor
    public Vector3d(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    // Get Methods
    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public double getZ() {
        return this.z;
    }

    // Returns Vector magnitude
    public double magnitude() {
        return Math.sqrt(this.x*this.x + this.y*this.y + this.z*this.z);
    }

    // Multiplies two vectors together and returns
    public double dotProduct(Vector3d vector) {
        return (this.x*vector.getX() + this.y*vector.getY() + this.z*vector.getZ());
    }

    // Adds two vectors and returns
    public Vector3d add(Vector3d vector) {
        double x = this.x + vector.getX();
        double y = this.y + vector.getY();
        double z = this.z + vector.getZ();
        Vector3d newVector = new Vector3d(x, y, z);
        return newVector;
    }
}
