/*
 * Thomas Roos
 * ID: 2139198
 */

package linearalgebra;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class Vector3dTests {

    private static Vector3d vect = new Vector3d(1.0,2.0,3.0);

    // Get Methods
    @Test
    public void getMethodsTests() {
        assertEquals(1.0, vect.getX(), 0);
        assertEquals(2.0, vect.getY(), 0);
        assertEquals(3.0, vect.getZ(), 0);
    }

    @Test
    // Magnitude Method
    public void getMagnitude() {
        assertEquals(3.7416573867739413, vect.magnitude(), 0);
    }

    // DotProduct Method
    @Test
    public void getDotProduct() {
        Vector3d vect2 = new Vector3d(2.0,3.0,4.0);
        assertEquals(20, vect.dotProduct(vect2), 0);
    }

    // Add Method
    @Test
    public void addTest() {
        Vector3d vect2 = new Vector3d(2.0,3.0,4.0);
        assertEquals(3.0, vect.add(vect2).getX(), 0);
        assertEquals(5.0, vect.add(vect2).getY(), 0);
        assertEquals(7.0, vect.add(vect2).getZ(), 0);
    }
}
